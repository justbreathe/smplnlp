# smplNLP

A small Kotlin library built on top of Stanford CoreNLP makes it easy to traverse dependency trees and find co-references between the
words.

It is licensed under GPL v3 because Stanford CoreNLP is also licensed under these terms.

## Usage example

For example, to find out all co-references for a word:

```kotlin
fun findCoreferences(candidate: Word) {
    val res = SmplNLPDepParser.depParse("Marie was born in Paris. She loves the museums.")
    for (sentence in res.sentenceList) {
        val corefChains = res.findCorefChains(WordPosition(sentence.position, candidate.position))
        if (corefChains.isNotEmpty()) {
            val firstAndOnlyCoref = corefChains.first()
            firstAndOnlyCoref.mainMention.rawMention // "Marie"
            firstAndOnlyCoref.mentions[0].rawMention // "Marie"
            firstAndOnlyCoref.mentions[1].rawMention // "She"
        }
    }
}
```
