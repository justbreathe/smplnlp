package io.jbreathe.nlp.smplnlp.dp

import io.jbreathe.nlp.smplnlp.WordPosition
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class SmplNLPDepParserTest {
    @Test
    internal fun `should not fail if null is passed`() {
        val res = SmplNLPDepParser.depParse(null)
        assertNotNull(res)
        assertTrue(res.sentenceList.isEmpty())
        assertTrue(res.findCorefChains(WordPosition(1, 1)).isEmpty())
    }

    @Test
    internal fun `should resolve co-references`() {
        val res = SmplNLPDepParser.depParse("Marie was born in Paris. She loves the museums.")
        assertEquals(2, res.sentenceList.size)
        val corefChains = res.findCorefChains(WordPosition(1, 1))
        assertEquals(1, corefChains.size)
        val corefChain = corefChains.first()
        assertEquals("Marie", corefChain.mainMention.rawMention)
        assertEquals(2, corefChain.mentions.size)
        assertEquals("Marie", corefChain.mentions[0].rawMention)
        assertEquals("She", corefChain.mentions[1].rawMention)
    }
}
