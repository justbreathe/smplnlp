package io.jbreathe.nlp.smplnlp.serialization

import com.google.common.graph.ValueGraphBuilder
import io.jbreathe.nlp.smplnlp.Word
import io.jbreathe.nlp.smplnlp.dp.PTBPosTag
import io.jbreathe.nlp.smplnlp.dp.UDRelation
import kotlinx.serialization.json.Json
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class ValueGraphSerializerTest {
    @Test
    internal fun `should serialize and deserialize graph`() {
        val valueGraph = ValueGraphBuilder
            .directed()
            .build<Word, UDRelation>()
        val word1 = Word("One", "one", PTBPosTag.CD, 1)
        val word2 = Word("two", "two", PTBPosTag.CD, 2)
        val word3 = Word("three", "three", PTBPosTag.CD, 3)
        val dot = Word(".", ".", PTBPosTag.DOT, 4)
        valueGraph.addNode(word1)
        valueGraph.addNode(word2)
        valueGraph.addNode(word3)
        valueGraph.addNode(dot)
        valueGraph.putEdgeValue(word1, word2, UDRelation.ACL)
        valueGraph.putEdgeValue(word1, word3, UDRelation.ADVCL)
        valueGraph.putEdgeValue(word3, dot, UDRelation.PUNCT)
        val serializer = ValueGraphSerializer(Word.serializer(), UDRelation.serializer())
        val jsonValue = Json.encodeToJsonElement(serializer, valueGraph)
        val decodedGraph = Json.decodeFromJsonElement(serializer, jsonValue)
        assertEquals(valueGraph, decodedGraph)
    }
}
