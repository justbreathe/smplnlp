package io.jbreathe.nlp.smplnlp.dp

import kotlinx.serialization.Serializable

/**
 * Part-of-speech tags used in the Penn Treebank Project.
 */
@Serializable
enum class PTBPosTag(val tagName: String) {
    CC("CC"), // Coordinating conjunction
    CD("CD"), // Cardinal number
    DT("DT"), // Determiner
    EX("EX"), // Existential there
    FW("FW"), // Foreign word
    IN("IN"), // Preposition or subordinating conjunction
    JJ("JJ"), // Adjective
    JJR("JJR"), // Adjective, comparative
    JJS("JJS"), // Adjective, superlative
    LS("LS"), // List item marker
    MD("MD"), // Modal
    NN("NN"), // Noun, singular or mass
    NNS("NNS"), // Noun, plural
    NNP("NNP"), // Proper noun, singular
    NNPS("NNPS"), // Proper noun, plural
    PDT("PDT"), // Predeterminer
    POS("POS"), // Possessive ending
    PRP("PRP"), // Personal pronoun
    `PRP$`("PRP$"), // Possessive pronoun
    RB("RB"), // Adverb
    RBR("RBR"), // Adverb, comparative
    RBS("RBS"), // Adverb, superlative
    RP("RP"), // Particle
    SYM("SYM"), // Symbol
    TO("TO"), // to
    UH("UH"), // Interjection
    VB("VB"), // Verb, base form
    VBD("VBD"), // Verb, past tense
    VBG("VBG"), // Verb, gerund or present participle
    VBN("VBN"), // Verb, past participle
    VBP("VBP"), // Verb, non-3rd person singular present
    VBZ("VBZ"), // Verb, 3rd person singular present
    WDT("WDT"), // Wh-determiner
    WP("WP"), // Wh-pronoun
    `WP$`("WP$"), // Possessive wh-pronoun
    WRB("WRB"), // Wh-adverb
    DOT(".");

    companion object {
        fun byTagName(tagName: String): PTBPosTag {
            when (tagName) {
                "." -> return DOT
            }
            return valueOf(tagName)
        }
    }
}
