package io.jbreathe.nlp.smplnlp.dp

import kotlinx.serialization.Serializable

/**
 * Universal Dependencies v2.
 */
@Serializable
enum class UDRelation(val relName: String) {
    NSUBJ("nsubj"),
    OBJ("obj"),
    IOBJ("iobj"),
    CSUBJ("csubj"),
    CCOMP("ccomp"),
    XCOMP("xcomp"),
    OBL("obl"),
    VOCATIVE("vocative"),
    EXPL("expl"),
    DISLOCATED("dislocated"),
    ADVCL("advcl"),
    ADVMOD("advmod"),
    DISCOURSE("discourse"),
    AUX("aux"),
    COP("cop"),
    MARK("mark"),
    NMOD("nmod"),
    APPOS("appos"),
    NUMMOD("nummod"),
    ACL("acl"),
    AMOD("amod"),
    DET("det"),
    CLF("clf"),
    CASE("case"),
    CONJ("conj"),
    CC("cc"),
    FIXED("fixed"),
    FLAT("flat"),
    COMPOUND("compound"),
    LIST("list"),
    PARATAXIS("parataxis"),
    ORPHAN("orphan"),
    GOESWITH("goeswith"),
    REPARANDUM("reparandum"),
    PUNCT("punct"),
    ROOT("root"),
    DEP("dep"),

    // subtypes
    NSUBJ_PASS("nsubj:pass"),
    AUX_PASS("aux:pass");
    // ... and more subtypes

    companion object {
        fun byRelName(relName: String): UDRelation {
            return valueOf(relName.replace(':', '_').uppercase())
        }
    }
}
