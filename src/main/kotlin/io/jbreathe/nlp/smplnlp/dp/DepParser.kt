package io.jbreathe.nlp.smplnlp.dp

import io.jbreathe.nlp.smplnlp.SentenceSequence

interface DepParser {
    fun depParse(text: String?): SentenceSequence<UDRelation>
}
