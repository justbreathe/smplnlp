package io.jbreathe.nlp.smplnlp

import com.google.common.graph.ValueGraph
import kotlinx.collections.immutable.toImmutableList
import java.util.regex.Pattern

data class Sentence<T>(
    val rawText: String,
    private val dependencyGraph: ValueGraph<Word, T>,
    val roots: List<Word>,
    val wordList: List<Word>,
    // Sentence pos in the text
    val position: Int
) {
    private val matcher by lazy { PATTERN.matcher(rawText) }

    fun children(node: Word): Set<Word> {
        return dependencyGraph.successors(node)
    }

    fun childWithEdgeValue(node: Word, edgeValue: T): Word? {
        val successors = dependencyGraph.successors(node)
        for (successor in successors) {
            if (edgeValue == edgeValue(node, successor)) {
                return successor
            }
        }
        return null
    }

    fun edgeValue(nodeU: Word, nodeV: Word): T? {
        return dependencyGraph.edgeValue(nodeU, nodeV).orElse(null)
    }

    fun allDirectSpeeches(): List<String> {
        if (!matcher.find()) {
            return emptyList()
        }
        val speeches = mutableListOf<String>()
        while (matcher.find()) {
            val group = matcher.group()
            speeches.add(group.substring(1, group.length - 1))
        }
        return speeches.toImmutableList()
    }

    companion object {
        private val PATTERN = Pattern.compile("\".+\"")
    }
}
