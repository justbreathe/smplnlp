package io.jbreathe.nlp.smplnlp

import io.jbreathe.nlp.smplnlp.dp.PTBPosTag
import io.jbreathe.nlp.smplnlp.serialization.Positionable
import kotlinx.serialization.Serializable

@Serializable
data class Word(
    val rawWord: String, val lemma: String, val partOfSpeech: PTBPosTag,
    val position: Int
) : Positionable {
    // Word pos in the sentence
    override fun position(): Int {
        return position
    }
}
