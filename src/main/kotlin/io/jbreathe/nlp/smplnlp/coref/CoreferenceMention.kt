package io.jbreathe.nlp.smplnlp.coref

import io.jbreathe.nlp.smplnlp.Word
import io.jbreathe.nlp.smplnlp.WordPosition
import kotlinx.serialization.Serializable

@Serializable
data class CoreferenceMention(val rawMention: String, val headWord: Word, val headWordPosition: WordPosition)
