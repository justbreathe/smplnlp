package io.jbreathe.nlp.smplnlp.coref

import kotlinx.serialization.Serializable

@Serializable
data class CoreferenceChain(val mainMention: CoreferenceMention, val mentions: List<CoreferenceMention>)
