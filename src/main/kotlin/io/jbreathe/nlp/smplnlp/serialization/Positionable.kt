package io.jbreathe.nlp.smplnlp.serialization

interface Positionable {
    fun position(): Int
}
