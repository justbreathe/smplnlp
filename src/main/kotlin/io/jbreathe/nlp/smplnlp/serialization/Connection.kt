package io.jbreathe.nlp.smplnlp.serialization

import kotlinx.serialization.Serializable

@Serializable
data class Connection<T>(val childId: Int, val edgeValue: T)
