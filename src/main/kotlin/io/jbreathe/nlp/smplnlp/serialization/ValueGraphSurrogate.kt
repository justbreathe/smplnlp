package io.jbreathe.nlp.smplnlp.serialization

import kotlinx.serialization.Serializable

@Serializable
data class ValueGraphSurrogate<N, T>(val nodes: List<N>, val adjacency: Map<Int, Set<Connection<T>>>)
