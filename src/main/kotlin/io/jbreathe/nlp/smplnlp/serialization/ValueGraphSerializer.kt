package io.jbreathe.nlp.smplnlp.serialization

import com.google.common.graph.ValueGraph
import com.google.common.graph.ValueGraphBuilder
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

class ValueGraphSerializer<N : Positionable, T>(
    private val nodeSerializer: KSerializer<N>,
    private val dataSerializer: KSerializer<T>
) : KSerializer<ValueGraph<N, T>> {
    override val descriptor: SerialDescriptor =
        ValueGraphSurrogate.serializer(nodeSerializer, dataSerializer).descriptor

    override fun deserialize(decoder: Decoder): ValueGraph<N, T> {
        val surrogate = decoder.decodeSerializableValue(ValueGraphSurrogate.serializer(nodeSerializer, dataSerializer))
        // note: immutability is not necessary here, because 'valueGraph' is not part of the public API
        val valueGraph = ValueGraphBuilder
            .directed()
            .build<N, T>()
        for ((nodeId, connections) in surrogate.adjacency) {
            val node = surrogate.nodes[nodeId - 1]
            valueGraph.addNode(node)
            for ((childId, edgeValue) in connections) {
                val childNode = surrogate.nodes[childId - 1]
                valueGraph.putEdgeValue(node, childNode, edgeValue)
            }
        }
        return valueGraph
    }

    override fun serialize(encoder: Encoder, value: ValueGraph<N, T>) {
        val roots = value.nodes().filter { value.inDegree(it) == 0 }
        val adjacency = mutableMapOf<Int, Set<Connection<T>>>()
        for (root in roots) {
            visitNode(root, value, adjacency)
        }
        val nodes = value.nodes().toList().sortedBy { it.position() }
        val surrogate = ValueGraphSurrogate(nodes, adjacency)
        encoder.encodeSerializableValue(ValueGraphSurrogate.serializer(nodeSerializer, dataSerializer), surrogate)
    }

    private fun visitNode(
        root: N, valueGraph: ValueGraph<N, T>, adjacency: MutableMap<Int, Set<Connection<T>>>
    ) {
        val connections = mutableSetOf<Connection<T>>()
        val children = valueGraph.successors(root)
        for (child in children) {
            val edgeValue = valueGraph.edgeValue(root, child).orElse(null)
            connections.add(Connection(child.position(), edgeValue))
            visitNode(child, valueGraph, adjacency)
        }
        adjacency[root.position()] = connections
    }
}
