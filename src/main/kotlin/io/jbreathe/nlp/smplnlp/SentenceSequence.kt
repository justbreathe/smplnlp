package io.jbreathe.nlp.smplnlp

import io.jbreathe.nlp.smplnlp.coref.CoreferenceChain
import io.jbreathe.nlp.smplnlp.coref.CoreferenceMention
import kotlinx.collections.immutable.toImmutableSet

data class SentenceSequence<T>(
    val sentenceList: List<Sentence<T>>,
    private val coreferenceChains: Set<CoreferenceChain>
) {
    fun findCorefChains(position: WordPosition): Set<CoreferenceChain> {
        return coreferenceChains.filter { chain: CoreferenceChain ->
            chain.mentions.any { mention: CoreferenceMention -> position == mention.headWordPosition }
        }.toImmutableSet()
    }
}
