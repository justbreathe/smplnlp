package io.jbreathe.nlp.smplnlp

import kotlinx.serialization.Serializable

@Serializable
data class WordPosition(private val sentenceNumber: Int, private val wordNumber: Int)
